# JSON Schemer Lint

JSON Schemer Lint is a CLI tool that surfaces any deviations
between JSON files and the JSON schema specification, as
set by [json-schema.org](https://json-schema.org/).

## Supported JSON Schema Drafts

At the moment, only **Draft 2019-09** is supported by JSON Schemer Lint.

## Installation

Prerequisites:

* **Node.js** - This tool was built with v12.8.1, though earlier versions might
  be supported.
* **NPM** or **yarn** - You can use whichever package manager you prefer.

Install using Node Package Manager (NPM):

```
npm install json-schemer-lint --save-dev
```

Install using Yarn:

```
yarn add json-schemer-lint --dev
```

## Usage

Lint a single JSON file using the CLI:

```
json-schemer-lint example.json
```

You can also lint a directory containing JSON files:

```
jsons-schemer-lint directory_with_json/
```

## Versioning

This project uses [SemVer](http://semver.org/) for versioning.

## License

This project is licensed under the MIT license.




