const cli = require("../lib/cli");

/******************************************************************************/
// Helpers
/******************************************************************************/

const testCliOnJson = (fixture, optionsList = []) => {
  return cli([
    "/path/to/bin/node",
    "/path/to/bin/json-schemer-lint",
    `tests/fixtures/${fixture}`,
    ...optionsList,
  ]);
};

/******************************************************************************/
// Tests
/******************************************************************************/

/**
 * This object contains all of the linting issues expected when linting the
 * fixtures/invalid directory.
 */
const expectedLintingIssues = {
  "tests/fixtures/invalid/allOfNonEmptyArray1.json": {
    "<root>": [
      "The allOf keyword's value must be a non-empty array.",
    ],
  },
  "tests/fixtures/invalid/allOfNonEmptyArray2.json": {
    "<root>": [
      "The allOf keyword's value must be a non-empty array.",
    ],
  },
  "tests/fixtures/invalid/allOfNonEmptyArray3.json": {
    "properties.A": [
      "The allOf keyword's value must be a non-empty array.",
    ],
  },
  "tests/fixtures/invalid/allOfNonEmptyArray4.json": {
    "properties.A.allOf[0]": [
      "The schema must not be empty.",
    ],
    "properties.A.allOf[1]": [
      "The schema must not be empty.",
    ],
    "properties.B.items.allOf[0]": [
      "The schema must not be empty.",
    ],
  },
  "tests/fixtures/invalid/anyOfNonEmptyArray.json": {
    "properties.A.anyOf[0]": [
      "The schema must not be empty.",
    ],
    "properties.A.anyOf[1]": [
      "The schema must not be empty.",
    ],
    "properties.B.items.anyOf[0]": [
      "The schema must not be empty.",
    ],
  },
  "tests/fixtures/invalid/commentValueIsString.json": {
    "<root>": [
      "The $comment keyword's value must be a string.",
    ],
    "properties.A": [
      "The $comment keyword's value must be a string.",
    ],
    "properties.B.items": [
      "The $comment keyword's value must be a string.",
    ],
  },
  "tests/fixtures/invalid/defsObjectValuesAreObjects.json": {
    "<root>": [
      "The $defs object's values must be objects (schemas).",
    ],
    "properties.A": [
      "The $defs object's values must be objects (schemas).",
    ],
    "properties.A.$defs.A": [
      "The schema must be an object or a boolean.",
    ],
    "properties.B": [
      "The $defs object's values must be objects (schemas).",
    ],
    "properties.B.$defs.A": [
      "The schema must be an object or a boolean.",
    ],
    "$defs.C": [
      "The schema must be an object or a boolean.",
    ],
  },
  "tests/fixtures/invalid/defsValueIsObject.json": {
    "<root>": [
      "The $defs keyword's value must be an object.",
    ],
    "properties.A": [
      "The $defs keyword's value must be an object.",
    ],
    "properties.B": [
      "The $defs keyword's value must be an object.",
    ],
  },
  "tests/fixtures/invalid/dependentSchemasValueIsObject.json": {
    "<root>": [
      "The dependentSchema keyword's value must be an object.",
    ],
    "properties.A.dependentSchemas.A": [
      "The schema must not be empty.",
    ],
  },
  "tests/fixtures/invalid/elseShouldHaveIf.json": {
    "<root>": [
      "The else keyword should have an if keyword as a counterpart.",
      "The else keyword's value must be a schema.",
    ],
    "properties.A": [
      "The else keyword should have an if keyword as a counterpart.",
      "The else keyword's value must be a schema.",
    ],
    "properties.B.items": [
      "The else keyword should have an if keyword as a counterpart.",
      "The else keyword's value must be a schema.",
    ],
  },
  "tests/fixtures/invalid/elseValueIsSchema.json": {
    "<root>": [
      "The else keyword's value must be a schema.",
    ],
    "properties.A": [
      "The else keyword's value must be a schema.",
    ],
    "properties.B.items": [
      "The else keyword's value must be a schema.",
    ],
  },
  "tests/fixtures/invalid/idKeyIsString.json": {
    "<root>": [
      "The $id keyword's value must be a string.",
      "The root schema $id should be an absolute URI.",
    ],
    "properties.A": [
      "The $id keyword's value must be a string.",
    ],
  },
  "tests/fixtures/invalid/idKeyOnRoot.json": {
    "<root>": [
      "The root schema should have an $id keyword.",
    ],
  },
  "tests/fixtures/invalid/idKeyOnRootIsUri.json": {
    "<root>": [
      "The root schema $id should be an absolute URI.",
    ],
  },
  "tests/fixtures/invalid/ifValueIsSchema1.json": {
    "<root>": [
      "The if keyword's value must be a schema.",
    ],
    "properties.A": [
      "The if keyword's value must be a schema.",
    ],
    "properties.B.items": [
      "The if keyword's value must be a schema.",
    ],
  },
  "tests/fixtures/invalid/ifValueIsSchema2.json": {
    "if": [
      "The schema must not be empty.",
    ],
    "properties.A.if": [
      "The schema must not be empty.",
    ],
    "properties.B.items.if":  [
      "The schema must not be empty.",
    ],
  },
  "tests/fixtures/invalid/notValueIsSchema1.json": {
    "<root>": [
      "The not keyword's value must be a schema.",
    ],
    "properties.A": [
      "The not keyword's value must be a schema.",
    ],
    "properties.B.items": [
      "The not keyword's value must be a schema.",
    ],
  },
  "tests/fixtures/invalid/notValueIsSchema2.json": {
    "not": [
      "The schema must not be empty.",
    ],
    "properties.A.not": [
      "The schema must not be empty.",
    ],
    "properties.B.items.not": [
      "The schema must not be empty.",
    ],
  },
  "tests/fixtures/invalid/oneOfNonEmptyArray.json": {
    "properties.A.oneOf[0]": [
      "The schema must not be empty.",
    ],
    "properties.A.oneOf[1]": [
      "The schema must not be empty.",
    ],
    "properties.B.items.oneOf[0]": [
      "The schema must not be empty.",
    ],
  },
  "tests/fixtures/invalid/refKeyIsString.json": {
    "properties.A": [
      "The $ref keyword's value must be a string.",
      "The schema must have a type keyword.",
    ],
    "properties.B.items.properties.A": [
      "The $ref keyword's value must be a string.",
      "The schema must have a type keyword.",
    ],
  },
  "tests/fixtures/invalid/schemaKeyIsUri.json": {
    "<root>": [
      "The $schema keyword must be a URI indicating the draft of the JSON schema standard.",
    ],
  },
  "tests/fixtures/invalid/schemaKeyOnRootOnly.json": {
    "properties.A": [
      "Subschemas are not allowed to have a $schema keyword.",
    ],
  },
  "tests/fixtures/invalid/schemaIsObjectOrBoolean1.json": {
    "<root>": [
      "The schema must be an object or a boolean.",
    ],
  },
  "tests/fixtures/invalid/schemaIsObjectOrBoolean2.json": {
    "<root>": [
      "The schema must be an object or a boolean.",
    ],
  },
  "tests/fixtures/invalid/thenShouldHaveIf.json": {
    "<root>": [
      "The then keyword should have an if keyword as a counterpart.",
      "The then keyword's value must be a schema.",
    ],
    "properties.A": [
      "The then keyword should have an if keyword as a counterpart.",
      "The then keyword's value must be a schema.",
    ],
    "properties.B.items": [
      "The then keyword should have an if keyword as a counterpart.",
      "The then keyword's value must be a schema.",
    ],
  },
  "tests/fixtures/invalid/thenValueIsSchema.json": {
    "<root>": [
      "The then keyword's value must be a schema.",
    ],
    "properties.A": [
      "The then keyword's value must be a schema.",
    ],
    "properties.B.items": [
      "The then keyword's value must be a schema.",
    ],
  },
  "tests/fixtures/invalid/typeKeyInvalid.json": {
    "<root>": [
      "The type keyword must be one of: array, boolean, integer, number, object, string.",
    ],
    "properties.A": [
      "The type keyword must be one of: array, boolean, integer, number, object, string.",
    ],
    "properties.B.properties.A": [
      "The type keyword must be one of: array, boolean, integer, number, object, string.",
    ],
    "properties.C.items": [
      "The type keyword must be one of: array, boolean, integer, number, object, string.",
    ],
  },
  "tests/fixtures/invalid/typeKeyRequired1.json": {
    "<root>": [
      "The schema must have a type keyword.",
    ],
    "properties.A": [
      "The schema must have a type keyword.",
    ],
    "properties.B": [
      "The schema must have a type keyword.",
    ],
  },
  "tests/fixtures/invalid/typeKeyRequired2.json": {
    "allOf[0]": [
      "The schema must have a type keyword.",
    ],
    "allOf[0].properties.A": [
      "The schema must have a type keyword.",
    ],
    "allOf[0].properties.B": [
      "The schema must have a type keyword.",
    ],
  },
  "tests/fixtures/invalid/typeKeyIsStringOrArray.json": {
    "<root>": [
      "The type keyword's value must be a string or array.",
    ],
    "properties.A": [
      "The type keyword's value must be a string or array.",
    ],
    "properties.B.properties.A": [
      "The type keyword's value must be a string or array.",
    ],
    "properties.B.properties.B.items": [
      "The type keyword's value must be a string or array.",
    ],
    "properties.C.contains": [
      "The type keyword's value must be a string or array.",
    ],
  },
  "tests/fixtures/invalid/unrecognizedKeywords.json": {
    "<root>": [
      "Unrecognized keywords were found found in schema: badKey",
    ],
    "properties.A": [
      "Unrecognized keywords were found found in schema: anotherBadKey",
    ],
    "properties.A.properties.A": [
      "Unrecognized keywords were found found in schema: yetAnotherBadKey",
    ],
  },
  "tests/fixtures/invalid/vocabObjectKeysAreUris.json": {
    "<root>":  [
      "The $vocabulary object's properties must be URIs.",
    ],
  },
  "tests/fixtures/invalid/vocabObjectValuesAreBooleans.json": {
    "<root>": [
      "The $vocabulary object's values must be booleans.",
    ],
  },
  "tests/fixtures/invalid/vocabValueIsObject.json":  {
    "<root>":  [
      "The $vocabulary keyword's value must be an object.",
    ],
  },
};

describe("JSON validation", () => {
  it("accepts valid JSON", () => {
    const errors = testCliOnJson("valid");
    expect(errors).toEqual({});
  });

  it("rejects invalid JSON", () => {
    const errors = testCliOnJson("invalid");
    expect(errors).toEqual(expectedLintingIssues);
  });

  it("rejects invalid JSON for draft 2019-09", () => {
    let errors = testCliOnJson("invalid", ["--draft", "2019-09"]);
    expect(errors).toEqual(expectedLintingIssues);

    errors = testCliOnJson("invalid", ["-d", "2019-09"]);
    expect(errors).toEqual(expectedLintingIssues);
  });
});

