module.exports = {
  "env": {
    "node": true,
    "commonjs": true,
    "jest/globals": true,
  },
  "extends": "eslint:recommended",
  "globals": {
    "Atomics": "readonly",
    "process": true,
    "SharedArrayBuffer": "readonly",
  },
  "plugins": ["jest"],
  "parserOptions": {
    "ecmaVersion": 2018
  },
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "always"
    ],
    "comma-dangle": [
      "error",
      {
        "arrays": "always-multiline",
        "objects": "always-multiline",
        "imports": "never",
        "exports": "never",
        "functions": "ignore"
      }
    ],
    "arrow-parens": [
      "error",
      "as-needed"
    ],
    "object-curly-spacing": [
      "error",
      "always"
    ],
    "space-in-parens": [
      "error",
      "never"
    ],
    "no-use-before-define": [
      "error",
      {
        "functions": true,
        "classes": true,
      }
    ],
    "max-len": [
      "error",
      {
        "code": 150,
        "comments": 80,
      }
    ]
  }
};
