const { CODES, check, isArray, isPlainObject } = require("../../utils");

module.exports = {
  id: "allof-value-is-nonempty-array",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.allOf != undefined,
      validIf: () => isArray(schema.allOf) && schema.allOf.length > 0,
      message: "The allOf keyword's value must be a non-empty array.",
      code: CODES.ERROR,
    })
  ),
};
