const { CODES, check, isPlainObject } = require("../../utils");
const { rule: schemaIsObjectOrBoolean } = require("./schemaIsObjectOrBoolean");

module.exports = {
  id: "not-value-is-schema",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.not != undefined,
      validIf: () => schemaIsObjectOrBoolean(schema.not).valid,
      message: "The not keyword's value must be a schema.",
      code: CODES.ERROR,
    })
  ),
};
