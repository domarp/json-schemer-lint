const { CODES, check, isPlainObject } = require("../../utils");

module.exports = {
  id: "$defs-value-is-object",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.$defs != undefined,
      validIf: () => isPlainObject(schema.$defs),
      message: "The $defs keyword's value must be an object.",
      code: CODES.ERROR,
    })
  ),
};
