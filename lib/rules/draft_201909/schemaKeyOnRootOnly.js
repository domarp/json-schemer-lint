const { CODES, atSchemaRoot, check, isPlainObject } = require("../../utils");

module.exports = {
  id: "$schema-key-on-root-only",
  rule: (schema, depth) => (
    check({
      applyIf: () => isPlainObject(schema) && !atSchemaRoot(depth),
      validIf: () => schema.$schema == undefined,
      message: "Subschemas are not allowed to have a $schema keyword.",
      code: CODES.ERROR,
    })
  ),
};
