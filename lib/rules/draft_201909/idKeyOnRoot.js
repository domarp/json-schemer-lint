const { CODES, atSchemaRoot, check, isPlainObject } = require("../../utils");

module.exports = {
  id: "$id-key-on-root",
  rule: (schema, depth) => (
    check({
      applyIf: () => isPlainObject(schema) && atSchemaRoot(depth),
      validIf: () => schema.$id != undefined,
      message: "The root schema should have an $id keyword.",
      code: CODES.WARNING,
    })
  ),
};
