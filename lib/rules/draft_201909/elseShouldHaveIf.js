const { CODES, check, isPlainObject } = require("../../utils");

module.exports = {
  id: "else-should-have-if",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.else != undefined,
      validIf: () => schema.if != undefined,
      message: "The else keyword should have an if keyword as a counterpart.",
      code: CODES.WARNING,
    })
  ),
};
