const { CODES, check, isPlainObject } = require("../../utils");

module.exports = {
  id: "$comment-value-is-string",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.$comment != undefined,
      validIf: () => isPlainObject(schema.$comment),
      message: "The $comment keyword's value must be a string.",
      code: CODES.ERROR,
    })
  ),
};
