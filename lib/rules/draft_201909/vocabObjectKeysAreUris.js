const urlRegex = require("url-regex");

const { CODES, check } = require("../../utils");
const { rule: vocabValueIsObject } = require("./vocabValueIsObject");

module.exports = {
  id: "$vocabulary-object-keys-are-URIs",
  rule: schema => (
    check({
      applyIf: () => vocabValueIsObject(schema).applicable,
      validIf: () => Object.keys(schema.$vocabulary).every(key => urlRegex().test(key)),
      message: "The $vocabulary object's properties must be URIs.",
      code: CODES.ERROR,
    })
  ),
};
