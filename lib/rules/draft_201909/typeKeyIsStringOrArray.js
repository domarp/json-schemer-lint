const { CODES, check, isArray, isString } = require("../../utils");
const { rule: typeKeyRequired } = require("./typeKeyRequired");

module.exports = {
  id: "type-key-is-string-or-array",
  rule: schema => (
    check({
      applyIf: () => typeKeyRequired(schema).applicable,
      validIf: () => isArray(schema.type) || isString(schema.type),
      message: "The type keyword's value must be a string or array.",
      code: CODES.ERROR,
    })
  ),
};
