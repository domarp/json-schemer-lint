const { CODES, check, isPlainObject } = require("../../utils");

module.exports = {
  id: "dependent-schemas-value-is-object",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.dependentSchemas != undefined,
      validIf: () => isPlainObject(schema.dependentSchemas),
      message: "The dependentSchema keyword's value must be an object.",
      code: CODES.ERROR,
    })
  ),
};
