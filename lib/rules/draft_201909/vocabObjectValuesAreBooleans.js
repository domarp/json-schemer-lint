const { CODES, check, isBoolean } = require("../../utils");
const { rule: vocabValueIsObject } = require("./vocabValueIsObject");

module.exports = {
  id: "$vocabulary-object-values-are-booleans",
  rule: schema => (
    check({
      applyIf: () => vocabValueIsObject(schema).applicable,
      validIf: () => Object.values(schema.$vocabulary).every(value => isBoolean(value)),
      message: "The $vocabulary object's values must be booleans.",
      code: CODES.ERROR,
    })
  ),
};
