const { CODES, check, isBoolean, isPlainObject } = require("../../utils");

module.exports = {
  id: "schema-is-object-or-boolean",
  rule: schema => (
    check({
      validIf: () => isPlainObject(schema) || isBoolean(schema),
      message: "The schema must be an object or a boolean.",
      code: CODES.ERROR,
    })
  ),
};
