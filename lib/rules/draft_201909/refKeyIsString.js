const { CODES, check, isPlainObject, isString } = require("../../utils");

module.exports = {
  id: "$ref-key-is-string",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.$ref !== undefined,
      validIf: () => isString(schema.$ref),
      message: "The $ref keyword's value must be a string.",
      code: CODES.ERROR,
    })
  ),
};
