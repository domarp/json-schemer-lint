const { CODES, check, isPlainObject } = require("../../utils");
const { rule: schemaIsObjectOrBoolean } = require("./schemaIsObjectOrBoolean");

module.exports = {
  id: "if-value-is-schema",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.if != undefined,
      validIf: () => schemaIsObjectOrBoolean(schema.if).valid,
      message: "The if keyword's value must be a schema.",
      code: CODES.ERROR,
    })
  ),
};
