const { CODES, check } = require("../../utils");
const { rule: schemaIsNotEmptyObject } = require("./schemaIsNotEmptyObject");

module.exports = {
  id: "type-key-is-required",
  rule: schema => (
    check({
      applyIf: () => schemaIsNotEmptyObject(schema).applicable,
      validIf: () => schema.type != undefined,
      message: "The schema must have a type keyword.",
      code: CODES.ERROR,
    })
  ),
};
