const { CODES, atSchemaRoot, check, isPlainObject } = require("../../utils");

module.exports = {
  id: "$schema-key-on-root",
  rule: (schema, depth) => (
    check({
      applyIf: () => isPlainObject(schema) && atSchemaRoot(depth),
      validIf: () => schema.$schema != undefined,
      message: "The root schema should have a $schema keyword.",
      code: CODES.WARNING,
    })
  ),
};
