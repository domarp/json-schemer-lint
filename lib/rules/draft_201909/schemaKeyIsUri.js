const { CODES, check } = require("../../utils");
const { rule: schemaKeyOnRoot } = require("./schemaKeyOnRoot");

const regex = /https?:\/\/json-schema.org\/draft\/2019-09\/schema#?/;

module.exports = {
  id: "$schema-key-is-URI",
  rule: (schema, depth) => (
    check({
      applyIf: () => schemaKeyOnRoot(schema, depth).applicable,
      validIf: () => regex.test(schema.$schema),
      message: "The $schema keyword must be a URI indicating the draft of the JSON schema standard.",
      code: CODES.ERROR,
    })
  ),
};
