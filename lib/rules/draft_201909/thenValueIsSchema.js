const { CODES, check, isPlainObject } = require("../../utils");
const { rule: schemaIsObjectOrBoolean } = require("./schemaIsObjectOrBoolean");

module.exports = {
  id: "then-value-is-schema",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.then != undefined,
      validIf: () => schemaIsObjectOrBoolean(schema.then).valid,
      message: "The then keyword's value must be a schema.",
      code: CODES.ERROR,
    })
  ),
};
