const { CODES, check, isPlainObject } = require("../../utils");
const { rule: schemaIsObjectOrBoolean } = require("./schemaIsObjectOrBoolean");

module.exports = {
  id: "else-value-is-schema",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.else != undefined,
      validIf: () => schemaIsObjectOrBoolean(schema.else).valid,
      message: "The else keyword's value must be a schema.",
      code: CODES.ERROR,
    })
  ),
};
