const { CODES, check, isPlainObject, isString } = require("../../utils");

module.exports = {
  id: "$id-key-is-string",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.$id !== undefined,
      validIf: () => isString(schema.$id),
      message: "The $id keyword's value must be a string.",
      code: CODES.ERROR,
    })
  ),
};
