const { CODES, check, isPlainObject } = require("../../utils");

module.exports = {
  id: "then-should-have-if",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.then != undefined,
      validIf: () => schema.if != undefined,
      message: "The then keyword should have an if keyword as a counterpart.",
      code: CODES.WARNING,
    })
  ),
};
