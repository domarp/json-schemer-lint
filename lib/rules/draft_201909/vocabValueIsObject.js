const { CODES, check, isPlainObject } = require("../../utils");

module.exports = {
  id: "$vocabulary-value-is-object",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.$vocabulary != undefined,
      validIf: () => isPlainObject(schema.$vocabulary),
      message: "The $vocabulary keyword's value must be an object.",
      code: CODES.ERROR,
    })
  ),
};
