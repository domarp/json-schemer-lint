const { CODES, check, isString } = require("../../utils");
const { rule: typeKeyRequired } = require("./typeKeyRequired");

const TYPES = [
  "array",
  "boolean",
  "integer",
  "number",
  "object",
  "string",
];

module.exports = {
  id: "type-key-is-invalid",
  rule: schema => (
    check({
      applyIf: () => typeKeyRequired(schema).applicable && isString(schema.type),
      validIf: () => TYPES.includes(schema.type),
      message: `The type keyword must be one of: ${TYPES.join(", ")}.`,
      code: CODES.ERROR,
    })
  ),
};
