const { CODES, check, isEmpty, isPlainObject } = require("../../utils");

module.exports = {
  id: "schema-is-not-empty-object",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema),
      validIf: () => !isEmpty(schema),
      message: "The schema must not be empty.",
      code: CODES.ERROR,
    })
  ),
};
