const urlRegex = require("url-regex");

const { CODES, check } = require("../../utils");
const { rule: idKeyOnRoot } = require("./idKeyOnRoot");

module.exports = {
  id: "$id-key-root-is-URI",
  rule: (schema, depth) => (
    check({
      applyIf: () => idKeyOnRoot(schema, depth).applicable,
      validIf: () => urlRegex().test(schema.$id),
      message: "The root schema $id should be an absolute URI.",
      code: CODES.WARNING,
    })
  ),
};
