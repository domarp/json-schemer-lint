const { CODES, check, isPlainObject } = require("../../utils");

const KEYWORDS = [
  "$comment",
  "$defs",
  "$id",
  "$ref",
  "$schema",
  "$vocabulary",
  "additionalItems",
  "additionalProperties",
  "allOf",
  "anyOf",
  "contains",
  "dependentSchemas",
  "default",
  "definitions",
  "description",
  "else",
  "enum",
  "exclusiveMinimum",
  "format",
  "id", // TODO: Is this actually a whitelisted keyword?
  "if",
  "items",
  "maximum",
  "maxItems",
  "maxLength",
  "minimum",
  "minItems",
  "not",
  "oneOf",
  "pattern",
  "patternProperties",
  "properties",
  "required",
  "then",
  "title",
  "type",
];

/**
 * Identify all extraneous keywords in the schema.
 *
 * @param {object} schema The schema with extra keywords
 * @returns {string} A formatted string to display the linting issue
 */
const extraKeywords = schema => (
  Object.keys(schema).filter(key => !KEYWORDS.includes(key)).join(", ")
);

module.exports = {
  id: "unrecognized-keywords",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema),
      validIf: () => Object.keys(schema).every(key => KEYWORDS.includes(key)),
      message: `Unrecognized keywords were found found in schema: ${extraKeywords(schema)}`,
      code: CODES.WARNING,
    })
  ),
};
