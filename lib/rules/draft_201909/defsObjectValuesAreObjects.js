const { CODES, check, isPlainObject } = require("../../utils");
const { rule: defsValueIsObject } = require("./defsValueIsObject");

module.exports = {
  id: "$defs-object-values-are-objects",
  rule: schema => (
    check({
      applyIf: () => defsValueIsObject(schema).applicable,
      validIf: () => Object.values(schema.$defs).every(value => isPlainObject(value)),
      message: "The $defs object's values must be objects (schemas).",
      code: CODES.ERROR,
    })
  ),
};
