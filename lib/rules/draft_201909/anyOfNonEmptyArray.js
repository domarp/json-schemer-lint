const { CODES, check, isArray, isPlainObject } = require("../../utils");

module.exports = {
  id: "anyof-value-is-nonempty-array",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.anyOf != undefined,
      validIf: () => isArray(schema.anyOf) && schema.anyOf.length > 0,
      message: "The anyOf keyword's value must be a non-empty array.",
      code: CODES.ERROR,
    })
  ),
};
