const { CODES, check, isArray, isPlainObject } = require("../../utils");

module.exports = {
  id: "oneof-value-is-nonempty-array",
  rule: schema => (
    check({
      applyIf: () => isPlainObject(schema) && schema.oneOf != undefined,
      validIf: () => isArray(schema.oneOf) && schema.oneOf.length > 0,
      message: "The oneOf keyword's value must be a non-empty array.",
      code: CODES.ERROR,
    })
  ),
};
