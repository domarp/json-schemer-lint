/******************************************************************************/
// Constants
/******************************************************************************/

const CODES = {
  "INFO": 0,
  "SUCCESS": 1,
  "WARNING": 2,
  "ERROR": 3,
};

/******************************************************************************/
// Shared Validators
/******************************************************************************/

const {
  isArray,
  isBoolean,
  isEmpty,
  isPlainObject,
  isString,
} = require("lodash");

/**
 * Check if linting against root schema.
 *
 * @param {string} depth The depth of the schema
 * @returns {boolean} True if current schema is root schema
 */
const atSchemaRoot = depth => depth === "<root>";

/******************************************************************************/
// Rule Checker Function
/******************************************************************************/

/**
 * This is the generic function that checks if the schema has a linting issue.
 *
 * @param {object} opts
 * @param {function} opts.applyIf Function to determine if rule applies
 * @param {function} opts.validIf Function to determine if schema is valid
 * @param {message} opts.message Linting issue message if schema is invalid
 * @param {code} opts.code The linting issue code (see CODES above)
 * @returns {object} Object with check result data
 */
const check = opts => {
  if (opts.applyIf && !opts.applyIf()) {
    return { valid: true, code: CODES.SUCCESS, message: "", applicable: false };
  }

  if (opts.validIf()) {
    return { valid: true, code: CODES.SUCCESS, message: "", applicable: true };
  } else {
    return { valid: false, code: opts.code, message: opts.message, applicable: false };
  }
};

module.exports = {
  CODES,
  atSchemaRoot,
  check,
  isArray,
  isBoolean,
  isEmpty,
  isPlainObject,
  isString,
};
