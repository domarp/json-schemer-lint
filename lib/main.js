const fs = require("fs");
const path = require("path");
const glob = require("glob");
const { isArray, isPlainObject } = require("lodash");

const { getRulesByDraft } = require("./rules");

// TODO: Handle warnings vs errors (vs info?)
// TODO: Make more asynchronous?
// TODO: TypeScript conversion!
// TODO: Filter out ignored rules once ignored rules are supported

/******************************************************************************/
// Helpers
/******************************************************************************/

/**
 * Parse a JSON file before linting it against the JSON schema standard.
 *
 * @param {string} file The file to parse for JSON data
 * @returns {object} The parsed JSON as a plain old object
 */
const parseJsonFile = file => {
  try {
    const jsonBuffer = fs.readFileSync(path.resolve(file));
    return JSON.parse(jsonBuffer);
  } catch (_) {
    console.log(`Failed to parse JSON: ${file}`);
    process.exit(1);
  }
};

/**
 * Lint a given schema against the JSON schema standard.
 *
 * @param {object} schema Object that will be linted against JSON schema
 * @param {object} depth The schema location ("<root>"" or a path)
 * @returns {object} An object containing all identified linting issues at the
 * current depth
 */
const lintSchemaShallow = (schema, depth, options) => {
  const rules = getRulesByDraft(options.draft);
  const ruleIds = Object.keys(rules);

  return ruleIds.reduce((issues, ruleId) => {
    const issue = rules[ruleId](schema, depth);

    if (!issue.valid) {
      issues[depth] = issues[depth] || [];
      issues[depth].push(issue.message);
    }

    return issues;
  }, {});
};

/**
 * Strip out "<root>." from a schema path, if it is present.
 *
 * @param {string} path The path of the current schema
 */
const stripRoot = path => path.replace("<root>.", "");

/**
 * Recursively traverse a schema object and identify any linting issues.
 *
 * @param {object} schema Object that will be linted against JSON schema
 * @param {object} depth The schema location - either <root> or a path
 * @returns {object} An object containing identified linting issues
 */
const lintSchemaDeep = (schema, depth = "<root>", options) => {
  let result = lintSchemaShallow(schema, depth, options);

  ["properties", "$defs", "dependentSchemas"].forEach(keyword => {
    if (isPlainObject(schema[keyword])) {
      Object.entries(schema[keyword]).forEach(([property, subschema]) => {
        result = {
          ...result,
          ...lintSchemaDeep(subschema, stripRoot(`${depth}.${keyword}.${property}`), options),
        };
      });
    }
  });

  ["not", "if", "then", "else", "items", "contains"].forEach(keyword => {
    if (isPlainObject(schema[keyword])) {
      result = {
        ...result,
        ...lintSchemaDeep(schema[keyword], stripRoot(`${depth}.${keyword}`), options),
      };
    }
  });

  ["allOf", "anyOf", "oneOf", "items", "contains"].forEach(keyword => {
    if (isArray(schema[keyword])) {
      schema[keyword].forEach((subschema, index) => {
        result = {
          ...result,
          ...lintSchemaDeep(subschema, stripRoot(`${depth}.${keyword}[${index}]`), options),
        };
      });
    }
  });

  return result;
};

/**
 * Lint a JSON file to determine if it adheres to JSON schema standards.
 *
 * @param {string} file Relative path of file
 * @param {object} options An object containing passed-in CLI options
 * @returns {object} An object containing the issues identified when linting
 * the file
 */
const lintFile = (file, options) => {
  const schema = parseJsonFile(file);
  const lintResult = lintSchemaDeep(schema, "<root>", options);

  return Object.keys(lintResult).length > 0 ? { [file]: lintResult } : {};
};

/**
 * Lint a directory of JSON files.
 *
 * @param {string} directory Relative path of directory
 * @param {object} options An object containing passed-in CLI options
 * @returns {object} An object containing the issues identified when linting
 * the directory
 */
const lintDirectory = (directory, options) => {
  let files = glob.sync(`${directory}/**/*.json`);

  return files.reduce((issues, file) => (
    { ...issues, ...lintFile(file, options) }
  ), {});
};

/**
 * Log the result of the linting process to standard output.
 *
 * @param {object} issues An object containing identified linting issues
 * @returns {void}
 */
const logOutput = issues => {
  if (Object.keys(issues).length === 0) {
    console.log("No issues found!");
    return;
  }

  console.log("There are issues in the following JSON schemas:");
  Object.entries(issues).forEach(([file, issues]) => {
    console.log(`| ${file}:`);
    Object.entries(issues).forEach(([depth, depthIssues]) => {
      console.log(`|   ${depth}:`);
      depthIssues.forEach(issue => console.log(`|     ${issue}`));
    });
  });
};

/******************************************************************************/
// Public Interface
/******************************************************************************/

/**
 * This is the function executed by the CLI to kick off linting.
 *
 * @param {string} fileOrDirectory The relative path of the JSON file/directory
 * @param {object} options An object containing passed-in CLI options
 * @returns {void} Outputs the result of the linting to the terminal
 */
const execute = (fileOrDirectory, options) => {
  // TODO: Support taking plain objects as well as JSON files?
  const jsonPath = path.resolve(fileOrDirectory);
  let issues;

  if (fs.lstatSync(jsonPath).isFile()) {
    issues = lintFile(fileOrDirectory, options);
  } else {
    issues = lintDirectory(fileOrDirectory, options);
  }

  // TODO: Make output logging optional based on a --silent flag
  logOutput(issues);

  return issues;
};

module.exports = execute;
