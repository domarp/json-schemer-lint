const execute = require("./main");

/******************************************************************************/
// Helpers
/******************************************************************************/

const supportedDrafts = ["2019-09"];

/**
 * Extract all of the options specified by the CLI user.
 *
 * @param {string[]} args Options passed in via process.argv
 */
const extractOptions = args => (
  args.reduce((opts, currArg, currIndex) => {
    // Specify draft using --draft or -d
    if (currArg === "--draft" || currArg == "-d") {
      const draft = args[currIndex + 1];

      if (!supportedDrafts.includes(draft)) {
        console.log(`Invalid JSON schema draft. The following drafts are supported: ${supportedDrafts.join(", ")}.`);
        process.exit(1);
      }

      opts.draft = draft;
    }

    // TODO: Support --help or -h

    return opts;
  }, {
    draft: "2019-09",
  })
);

/******************************************************************************/
// Public Interface
/******************************************************************************/

/**
 * This is the entry point for the JSON Schemer Linting CLI.
 *
 * @param {string[]} args Passthrough of process.argv
 * @returns {object} All issues found by the linter
 */
const cli = args => {
  const jsonFileOrDirectory = args[2];
  const optionsList = args.slice(3);

  if (!jsonFileOrDirectory) {
    console.log("Please provide a .json file or a directory as an argument.");
    process.exit(1);
  }

  return execute(jsonFileOrDirectory, extractOptions(optionsList));
};

module.exports = cli;
