const glob = require("glob");

const RULES_DIR = {
  "2019-09": "draft_201909",
};

const getRulesByDraft = draft => (
  glob.sync(`${__dirname}/rules/${RULES_DIR[draft]}/**/*.js`)
    .reduce((rules, fileName) => {
      const { id, rule } = require(fileName);
      rules[id] = rule;
      return rules;
    }, {})
);

module.exports = {
  getRulesByDraft,
};
